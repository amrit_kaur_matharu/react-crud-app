import React from 'react';
import { Button } from 'react-bootstrap';
import Navbar  from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import NavDropdown from 'react-bootstrap/NavDropdown';
import Form from 'react-bootstrap/Form';
import FormControl from 'react-bootstrap/FormControl';
import {Link} from 'react-router-dom';

const header = () =>(
  <Navbar expand="xl" bg="dark" variant="dark">
    <Navbar.Brand href="/">CRUD Application</Navbar.Brand>
    <Navbar.Toggle aria-controls="basic-navbar-nav" />
    <Navbar.Collapse id="basic-navbar-nav">
      <Nav className="mr-auto">
        <Link className="nav-link" to="students">Students</Link>
        <Link className="nav-link" to="teachers">Teachers</Link>
        <Link className="nav-link" to="about">About</Link>
        <Link className="nav-link" to="contact">Contact</Link>
        <NavDropdown title="Search" id="basic-nav-dropdown">
          <NavDropdown.Item href="#action/3.1">Students</NavDropdown.Item>
          <NavDropdown.Divider />
          <NavDropdown.Item href="#action/3.2">Teachers</NavDropdown.Item>
          <NavDropdown.Divider />
        </NavDropdown>
      </Nav>
      <Form inline>
        <FormControl type="text" placeholder="Search" className="mr-sm-2" />
        <Button variant="outline-info">Search</Button>
      </Form>
    </Navbar.Collapse>
  </Navbar>
);

export default header;