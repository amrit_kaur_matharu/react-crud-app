import React, {Component} from 'react';

export default class StudentItem extends Component{
    constructor(props){
        super(props);
        this.state = {
            isEdit: false
        }
    }
    deleteStudent = () => {
        console.log("This is delete student!");
        let {id} = this.props.student;
        this.props.deleteStudent(id);
    }
    editStudent = () => {
        console.log("This is edit student function!");
        this.setState((prevState, props) => ({
            isEdit: !prevState.isEdit
        }))
    }
    updateStudent = () => {
        console.log("This is update student function!");
        const {id} = this.props.student;
        this.setState((prevState, props) => ({
            isEdit: !prevState.isEdit
        }));
        this.props.editStudent(
            id,
            this.nameInput.value,
            this.gradeInput.value,
            this.schoolInput.value
        );
    }
    render(){
        const {id, name, grade, school} = this.props.student;
        return(
            this.state.isEdit ? (
                <tr key={this.props.index}>
                     <td>{id}</td>
                    <td>
                        <input ref={nameInput => this.nameInput = nameInput }
                            defaultValue={name}/>
                    </td>
                    <td>
                        <input ref={gradeInput => this.gradeInput = gradeInput }
                            defaultValue={grade}/>
                    </td>
                    <td>
                        <input ref={schoolInput => this.schoolInput = schoolInput }
                            defaultValue={school}/>
                    </td>
                    <td onClick={this.updateStudent}>Update</td>
                    <td onClick={this.deleteStudent}>Delete</td>
                </tr>
            ) : (
            <tr key={this.props.index}>
                <td>{id}</td>
                <td>{name}</td>
                <td>{grade}</td>
                <td>{school}</td>
                <td onClick={this.editStudent}>Edit</td>
                <td onClick={this.deleteStudent}>Delete</td>
            </tr>
            )
        );
    }
}