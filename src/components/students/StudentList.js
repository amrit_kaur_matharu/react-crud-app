import React from "react";
import StudentItem from './StudentItem';

const TableHeader = () => {
    return(
        <thead>
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Grade</th>
                <th>School</th>
                <th>Edit/Save</th>
                <th>Delete</th>
            </tr>
        </thead>
    )
}
const Table = (props) =>{    
    let students = props.studentData;
    const studentRow = students.map((item, index) => (
        <StudentItem 
            key={index}
            student={item}
            index={index}
            deleteStudent={props.deleteStudent}
            editStudent={props.editStudent}
        />
    ))
    return(
        <table className="table table-hover">
            <TableHeader></TableHeader>
            <tbody>{studentRow}</tbody>
        </table>
    )    
}
export default Table;