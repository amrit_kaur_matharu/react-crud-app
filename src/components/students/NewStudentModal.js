import React, { Component } from 'react';
import { Modal } from 'react-bootstrap';

class LoginModal extends Component {
  constructor(props, context){
    super(props, context);    
    this.state = {
        show: false,
        student: {
          name: '',
          grade: '',
          school: ''
        }
    }
}
handleShow = () => {
    console.log(this.state)
    this.setState({ show: true })
}
handleClose = () => {
    this.setState({ show: false })
}
handleInputChange = (event) => {
  let student = this.state.student ? this.state.student : {};
  const target = event.target;
  const name = target.name;
  student[name] = event.target.value;  
  this.setState({student: student});
}
handleSubmit = (event) => {
  console.log("Student Form state Data - ", this.state.student);
  this.props.addNewStudent(this.state.student);
  this.setState({student:  {
    name: '',
    grade: '',
    school: ''
  }})
  this.handleClose();
  event.preventDefault();
}
render() {
    return (
       <div>
          <Modal show={this.state.show} onHide={this.handleClose}>
             <Modal.Header closeButton>
               <Modal.Title>Add New Student</Modal.Title>
             </Modal.Header>
             <Modal.Body>
               <form onSubmit={this.handleSubmit}>  
                <label> Student Name :           
                <input
                  name="name"
                  type="text"
                  value={this.state.student.name} 
                  placeholder="Enter Student Name" 
                  onChange={this.handleInputChange}
                  required 
                />
                </label>
                <label> Student Grdae :           
                <input
                  name="grade"
                  type="text"
                  value={this.state.student.grade} 
                  placeholder="Enter Student grade" 
                  onChange={this.handleInputChange}
                  required 
                />
                </label>
                <label> Student school :           
                <input
                  name="school"
                  type="text"
                  value={this.state.student.school} 
                  placeholder="Enter Student school" 
                  onChange={this.handleInputChange}
                  required 
                />
                </label>  
                <button>Go!</button>
              </form>
             </Modal.Body>
          </Modal>
        </div>
    )
  }
}
export default LoginModal