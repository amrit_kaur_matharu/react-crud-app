import React, { Component } from 'react';
import { Button } from 'react-bootstrap';
import StudentListTable from './StudentList';
import GetRequest, {postRequest as PostRequest, deleteRequest as DeleteRequest, putRequest as PutRequest} from '../../utils/API';
import LoadingSpinner from '../common/loader';
import LoginModal from './NewStudentModal';

class App extends Component{
    constructor(props){
      super(props);
      this.state = {
        studentList: [],
        isLoading: false,
      };
    }
    loginModalRef = ({handleShow}) => {
      this.showModal = handleShow;
   }
   
   onLoginClick = () => {
     this.showModal();
   }
    getStudents = () => {
      const options = {
        url: 'students'
      }
      this.setState({isLoading: true});
      GetRequest(options, (res) => {
        console.log("student list - ", res);
        this.setState({ studentList : res.data });
        this.setState({isLoading: false});
      });
    }
    addStudent = (studentobj) => {
      const options = {
        url : 'students',
        jsonData: studentobj
      }
      PostRequest(options, (res) => {
        if(res && res.data){
          this.setState({ studentList: [...this.state.studentList, ...[res.data]]});
        }
      })
    }
    componentDidMount() {
      this.getStudents();
    }
    
    editStudent = (id, name, grade, school) => {
      console.log("Edit Function in Parent!");
      
      let student = this.state.studentList.find( std => std.id === id);
      student.name = name;
      student.grade = grade;
      student.school = school;
      const options = {
        url: 'students/'+id,
        jsonData: student
      }
      PutRequest(options, res => {
        if(res && res.status === 200){
          let studentListCopy = this.state.studentList.map((student) => {    
            if (student.id === id) {
              student.name = name;
              student.grade = grade;
              student.school = school;
            }
            return student;
          });
          this.setState({
            studentList: studentListCopy
          });
        }
      });      
    }
    deleteStudent = (id) => {
      console.log("Delete function in parent class!");
      let response = window.confirm("Do you really want to delete this item?");
      if(response){
        const options = {
          url: 'students',
          id: id
        }
        DeleteRequest(options, (res) => {
          if(res && res.status === 200){
            console.log("Delete response - ", res);
            let filteredStudentList = this.state.studentList.filter( 
              x => x.id !== id
            );
            this.setState({studentList: filteredStudentList})
          }
        });        
      }
    }
    addNewStudent = (student) => {
      let studentListLocal = this.state.studentList;
      if(studentListLocal){  
        student.id =  Math.max(...studentListLocal.map(function(o){
                        return o.id
                      })) + 1 ; 
        this.addStudent(student);
      }
    }
    render(){
      const isLoading = this.state.isLoading;
        return (          
            <div className="App">
                { isLoading ? <LoadingSpinner/> :                
                <StudentListTable 
                  studentData={this.state.studentList}
                  deleteStudent={this.deleteStudent}
                  editStudent={this.editStudent}/>}   
                  <LoginModal ref={this.loginModalRef} addNewStudent={this.addNewStudent}></LoginModal>       
                  <Button variant="primary" className="mr-2" onClick={this.onLoginClick}>Add New Student</Button>      
            </div>
        ); 
    }
}
export default App;