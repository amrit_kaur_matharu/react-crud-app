import React from 'react';
import Spinner from 'react-bootstrap/Spinner'                       

const LoadingSpinner = () => (
    <div className="spinner-center">
        <Spinner animation="border" role="status">
            <span className="sr-only">Loading...</span>
        </Spinner>
    </div>
);

export default LoadingSpinner;