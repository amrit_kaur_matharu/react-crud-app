import axios from 'axios';
const baseUrl = 'http://localhost:3001/';

const getRequest = (options, callback) => {
    axios.get(baseUrl + options.url )
    .then(response => {
        callback(response);
    })
    .catch(error => {
        console.log(error);
        callback(error);
    });
}
export const putRequest = (options, callback) => {
    axios.put(baseUrl + options.url, options.jsonData )
    .then(response => {
        callback(response);
    })
    .catch(error => {
        console.log(error);
        callback(error);
    });
}
export const postRequest = (options, callback) => {
    axios.post(baseUrl + options.url, options.jsonData )
    .then(response => {
        callback(response);
    })
    .catch(error => {
        console.log(error);
        callback(error);
    });
}
export const deleteRequest = (options, callback) => {
    axios.delete(baseUrl + options.url + '/' + options.id )
    .then(response => {
        callback(response);
    })
    .catch(error => {
        console.log(error);
        callback(error);
    });
}
export default getRequest;