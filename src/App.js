import React, { Component } from 'react';
import Header from './components/header';
import Home from './components/home';
import Students from './components/students/students';
import Teachers from './components/teachers';
import About from './components/about';
import Contact from './components/contact';
import { BrowserRouter as Router, Route, Link, Switch } from 'react-router-dom'; 

const studentList = [
  {id:1,name:'John Doe',grade:1,school:'React Redux School'},
  {id:2,name:'Jane Doe',grade:2,school:'React Redux School'},  
  {id:3,name:'Terry Adams',grade:3,school:'React Redux School'},
  {id:4,name:'Jenny Smith',grade:4,school:'React Redux School'}
];

if(localStorage.getItem("students") == null || (localStorage.getItem("students") === "undefined")){
  localStorage.setItem("students", JSON.stringify(studentList));
}
class App extends Component{
    render(){
        return (
          <Router>
            <div className="App container">
                <Header></Header>                
                <Route exact path='/' component={Home}></Route> 
                <Route exact path='/students' component={Students}></Route> 
                <Route exact path='/teachers' component={Teachers}></Route>
                <Route exact path='/about' component={About}></Route> 
                <Route exact path='/contact' component={Contact}></Route>  
                </div>
          </Router>
        ); 
    }
}
export default App;